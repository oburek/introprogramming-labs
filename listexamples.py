colors=[
    "RED",
    "YELLOW",
    "BLUE",
    "ORANGE",
    "GREEN",
    "PURPLE"
    ]

def showTitle():
    print("Color Preference Evaluator")
    
def promptForColor():
    userColor=input("Please enter a color: ")
    return userColor.strip().upper()

def confirmColor(userColor):
    response=input("You entered "+userColor+". Is this correct (Y/N)?")
    response=response.upper()
    if response=="Y":
        return True
    else:
        return False
    
def containsElement(userColor):
    for i in colors:
        if userColor==i:
            return True
        else:
            continue
    return False
        
        
    return True

def praiseUser():
    print("Good Job!")
    
def ridiculeUser():
    print("You're thinking too hard about this. Get some water and try again.")
    
def main():
    showTitle()
    while True:
        userColor=promptForColor()
        if confirmColor(userColor):   
            if containsElement(userColor):
                praiseUser()
                break
            else:
                ridiculeUser()
        else:
            continue
    
main()
        
    
