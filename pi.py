#Owen Burek
#September 19, 2016
import math
def main():
    print("This program will try to calculate the value of pi.")
    pi=0
    denominator=1
    toSum=int(input("Enter the number of terms to sum: "))
    
    for i in range(0, toSum*2, 4):
        pi=(pi)+(4/denominator)
        denominator+=2
        
        pi=(pi)-(4/denominator)
        denominator+=2

        
    print("This program's approximated value of pi is:", pi)
    print("The difference between the approximated pi "
          "and math.pi is:" , (math.pi-pi))
        
        

main()
