def main():
    speedOfLight=186000.0
    positionDifference=34000000.0
    print("It takes approximately", positionDifference/speedOfLight, "seconds in order for a photo from Mars to reach Earth.")
    print("Please note, this calculation assumes that Mars is at its closest orbit to Earth.")
main()
