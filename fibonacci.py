#Owen Burek
#September 16, 2016

def main():
    print("Welcome to the Fibonacci number finder!")
    firstNumber=1
    secondNumber=1
    stopPoint=int(input("Enter a positive integer: "))
    for i in range (stopPoint-2):
        firstNumber, secondNumber=secondNumber, secondNumber+firstNumber
        print(secondNumber)
    print("The Fibonacci number you are looking for is", secondNumber)
main()
