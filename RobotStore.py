#Owen Burek
#Intro to Programming
#Lab #9

class Products:
    def __init__(self,name,price,quantity):
        self.name=name
        self.price=price
        self.quantity=quantity

    def inStock(self,quantity):
        print()
        print("Available Products")
        print("------------------")
        for i in range(0,len(allProducts)):
            if allProducts[i].quantity > 0:
                print(str(i)+")", allProducts[i].name, "$", allProducts[i].price)

    def cost(self,count,prodId):
        return allProducts[prodId].price*count

    def removeStock(self,count,prodId):
        allProducts[prodId].quantity-=count

allProducts=[
    Products("Ultrasonic range finder", 2.50, 4),
    Products("Servo Motor", 14.99, 10),
    Products("Servo controller", 44.95, 5),
    Products("Microcontroller board", 34.95, 7),
    Products("Laser range finder", 149.99, 2),
    Products("Lithium polymer battery", 8.99, 8)
    ]
    
def main():
    cash = float(input("How much money do you have? "))
    while cash > 0:
        Products.inStock(Products,allProducts)
        userInput=input("Enter product ID and quantity you wish to buy: ")
        inputStrings=userInput.split(" ")
        [prodId,count] = map(int,inputStrings)
        product=allProducts[prodId]
        if allProducts[prodId].quantity >= count:
            if cash >= allProducts[prodId].price:
                Products.removeStock(Products,count,prodId)
                cash-=Products.cost(Products,count,prodId)
                print("You purchased", count, allProducts[prodId].name+".")
                print("You have $",cash,"remaining.")
            else:
                print("Sorry, you cannot afford that product.")
        else:
            print("Sorry, we are sold out of", allProducts[prodId].name)
