#Owen Burek
#CMPT_120L_113_1F

class Locale:

    def __init__(self,name,description,item):
        self.name=name
        self.description=description
        self.isVisited=False
        self.item=item

    def render(self):
        if self.isVisited==False:
            print(self.description)
        else:
            print("You have returned to "+self.name+".")

    def removeItem(self):
        self.item=None

    def visit(self):
        self.isVisited=True
        
