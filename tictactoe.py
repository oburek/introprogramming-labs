#Owen Burek
#November 4, 2016
end=True
symbols=[" ","o","x"]
row=0
column=0
turn=0
space=[
      [0,0,0],
      [0,0,0],
      [0,0,0]
            ]

def boardStart():
    for i in range (3):
        for x in range (3):
            space[i][x]=symbols[0]

def boardBorders(row, column, player, space):
    for i in range (3):    
        print("+-----------+")
        print("|",space[i][0],"|",space[i][1] ,"|",space[i][2] ,"|")
    print("+-----------+")
    
def play(row, column, player, space):
    global turn
    if space[row][column]==symbols[0]:
       if player==1:
           space[row][column]=symbols[1]
           print("True")
           return True
       else:
           space[row][column]=symbols[2]
           print("True")
           return True
    else:
        turn+=1
        print("False")
        return False
            
def gameEnd():
    global end
    for i in range (3):
        if space[i][0]==symbols[0] or space[i][1]==symbols[0] or space[i][2]==symbols[0]:
            return True
    end=False
                
        
def main():
    global turn
    boardStart()
    while end:
        if turn%2==0:
            player=1
            turn+=1
        elif turn%2==1:
            player=2
            turn+=1
        row=int(input("Enter row: "))
        column=int(input("Enter colummn: "))
        row-=1
        column-=1
        play(row, column, player, space)
        boardBorders(row, column, player, space)
        gameEnd()
    print("GAME OVER")
     
main()

